cmake_minimum_required(VERSION 2.8.12)

project(PH-sensor LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_PREFIX_PATH  "C:/Qt/5.9.1/mingw53_32")
set(Qt5Charts_DIR "C:/Qt/5.9.1/mingw53_32/lib/cmake/Qt5Charts")

find_package(Qt5 COMPONENTS Core Quick Charts Serialport Qml REQUIRED)

set(BOOST_ROOT "c:/usr/local")
set(BOOST_INCLUDEDIR "C:/usr/local/include/boost-1_64")
find_package(Boost)
message(${Boost_INCLUDE_DIR})
add_executable(${PROJECT_NAME} "main.cpp" "resources.qrc" "datasource.h" "datasource.cpp")

target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Quick Qt5::Charts Qt5::SerialPort Qt5::Qml)
include_directories(${Boost_INCLUDE_DIRS})
