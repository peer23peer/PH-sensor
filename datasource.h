/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <QtCore/QObject>
#include <QtCharts/QAbstractSeries>
#include <QtSerialPort>
#include <vector>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <string>

QT_BEGIN_NAMESPACE
class QQuickView;
QT_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class DataSource : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double ActualValue READ getActualValue WRITE setActualValue NOTIFY ActualValueChanged)
    Q_PROPERTY(double ActualTime READ getActualTime WRITE setActualTime NOTIFY ActualTimeChanged)
    Q_PROPERTY(bool Connected READ getConnected WRITE setConnected NOTIFY ConnectedChanged)
    Q_PROPERTY(bool Exportable READ getExportable WRITE setExportable NOTIFY ExportableChanged)

public:
    explicit DataSource(QQuickView *appViewer, QObject *parent = 0);
    virtual ~DataSource();

    double getActualValue();
    void setActualValue(double value);
    double getActualTime();
    void setActualTime(double value);
    bool getConnected();
    void setConnected(bool value);
    bool getExportable();
    void setExportable(bool value);

private:
    void setSerialPort(); 

Q_SIGNALS:

public slots:
    void update(QAbstractSeries *series);
    void on_portChanged(const QString &portname);
    void on_refreshChanged(const int &rate);
    void connectSerial();
    void disconnectSerial();
    void readSerial();
    void prepData();
    void log();
    void stopLog();
    void exportToCsv(const QString &filename);

signals:
    void dataReady();
    void updateChart();
    void ActualValueChanged();
    void ActualTimeChanged();
    void ConnectedChanged();
    void ExportableChanged();

private:
    QSerialPort *serialPort;
    QQuickView *m_appViewer;
    QByteArray s_data;
    QVector<QPointF> p_data;
    boost::posix_time::ptime *start_time;
    double actual_time;
    double actual_value;
    bool Connected;
    bool sendToChart;

    std::string QPoint_to_String(QPointF &p);
};

#endif // DATASOURCE_H
