#include "datasource.h"
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts>
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>
#include <QtCore/QDebug>
#include <QtCore/QtMath>
#include <QSerialPortInfo>
#include <QDebug>
#include <QTimer>
#include <QThread>
#include <fstream>
#include <sstream>
#include <string>


QT_CHARTS_USE_NAMESPACE

Q_DECLARE_METATYPE(QAbstractSeries *)
Q_DECLARE_METATYPE(QAbstractAxis *)

DataSource::DataSource(QQuickView *appViewer, QObject *parent) :
    QObject(parent),
    m_appViewer(appViewer),
    Connected(false),
    sendToChart(false),
    actual_value(0.),
    actual_time(0.)
{
    qRegisterMetaType<QAbstractSeries*>();
    qRegisterMetaType<QAbstractAxis*>();

    QString portname = QSerialPortInfo::availablePorts().first().portName();
    serialPort = new QSerialPort(portname, this);

    QObject::connect(this, &DataSource::dataReady, this, &DataSource::prepData);
}

DataSource::~DataSource() {
    if (start_time) {
        delete start_time;
    }
    if (serialPort) {
        if (serialPort->isOpen()) {
            serialPort->write("h");
            QThread::msleep(500);
            serialPort->close();
        }
        delete serialPort;
    }
}

void DataSource::setSerialPort() {
    if (!serialPort->setBaudRate(QSerialPort::Baud9600)) {
        qDebug() << serialPort->errorString();
    }
    serialPort->setDataBits(QSerialPort::Data8);
}

void DataSource::on_portChanged(const QString &portname) {
    if (serialPort != NULL && serialPort->isOpen()) {
            QThread::msleep(500);
    }
    delete serialPort;
    serialPort = new QSerialPort(portname, this);
}

void DataSource::on_refreshChanged(const int &rate) {
    QByteArray data;
    data.append(static_cast<char>(rate));
    serialPort->write(data);
}

void DataSource::connectSerial() {
    if (!serialPort->open(QIODevice::ReadWrite)) {
        qDebug() << serialPort->errorString();
    }
    setSerialPort();
    QObject::connect(serialPort, &QSerialPort::readyRead, this, &DataSource::readSerial);
    setConnected(true);
    if (getExportable()) {
        s_data.clear();
        p_data.clear();
        emit ExportableChanged();
    }
    if (start_time) {
        delete start_time;
    }
    start_time = new boost::posix_time::ptime(boost::posix_time::microsec_clock::local_time());
    serialPort->write("s");
}

void DataSource::disconnectSerial() {
    serialPort->write("h");
    QThread::msleep(500);
    serialPort->close();
    setConnected(false);
}

void DataSource::readSerial() {
    QByteArray data = serialPort->readAll();
    for (auto d : data) {
        s_data.append(d);
    }
    if (s_data.size() >= 4) {
        emit dataReady();
    }
}

void DataSource::prepData() {
    while (s_data.size() >= 4) {
        unsigned int pint = ((unsigned char)s_data[3] << 24)|((unsigned char)s_data[2] << 16)|((unsigned char)s_data[1] << 8)|(unsigned char)s_data[0];
        float *p =  reinterpret_cast<float*>(&pint);
        boost::posix_time::time_duration time;
        time = boost::posix_time::ptime(boost::posix_time::microsec_clock::local_time()) - *start_time;
        actual_time = static_cast<double>(time.total_milliseconds()) / 1000.;
        setActualValue(*p);
        QPointF point(actual_time, actual_value);
        s_data.remove(0, 4);
        if (sendToChart) {
            p_data.append(point);
            emit updateChart();
        }
        if (p_data.size() == 1) {
            emit ExportableChanged();
        }
    }
}


void DataSource::update(QAbstractSeries *series)
{
    if (series) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        xySeries->replace(p_data);
    }
}

double DataSource::getActualValue() {
    return actual_value;
}

void DataSource::setActualValue(double value) {
    actual_value = value;
    emit ActualValueChanged();
}

double DataSource::getActualTime() {
    return actual_time;
}

void DataSource::setActualTime(double value) {
    actual_time = value;
    emit ActualTimeChanged();
}

bool DataSource::getConnected() {
    return Connected;
}

void DataSource::setConnected(bool value) {
    Connected = value;
    emit ConnectedChanged();
}

bool DataSource::getExportable() {
    if (p_data.size() > 0) {
        return true;
    } else {
        return false;
    }
}

void DataSource::setExportable(bool value) {
    emit ExportableChanged();
}

void DataSource::log() {
    p_data.clear();
    if (start_time) {
        delete start_time;
    }
    start_time = new boost::posix_time::ptime(boost::posix_time::microsec_clock::local_time());
    emit ExportableChanged();
    sendToChart = true;
}

void DataSource::stopLog() {
    sendToChart = false;
}

void DataSource::exportToCsv(const QString &filename) {
    std::ofstream file;
    file.open(filename.toStdString().substr(8));
    if (file.is_open()) {
        file << "time\tph_value" << std::endl;
        for (QPointF &p : p_data) {
            file << QPoint_to_String(p)  << std::endl;
        }
        file.close();
    }
    return;
}

std::string DataSource::QPoint_to_String(QPointF &p) {
    std::ostringstream ss;
    ss.imbue(std::locale());
    ss.precision(3);
    ss.setf(std::ostringstream::showpoint);
    ss << static_cast<double>(p.rx()) << "\t" << static_cast<double>(p.ry());
    std::string retVal = ss.str();
    std::replace(retVal.begin(), retVal.end(), '.', ',');
    return retVal;
}


